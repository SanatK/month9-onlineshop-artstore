package kg.attractor.artstore.model.Invoice;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Data
@Entity
@Table(name = "invoice_items")
public class InvoiceItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Integer itemId;

    @Column(length = 128)
    @NotBlank
    @Size(min = 1, max = 128)
    private String itemName;

    @Column
    private Integer quantity;

    @Column(length = 128)
    @NotBlank
    @Size(min = 1, max = 128)
    private String itemCategory;

    @Column
    private float pricePerOne;

    @Column
    private float price;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id")
    private Invoice invoice;
}
