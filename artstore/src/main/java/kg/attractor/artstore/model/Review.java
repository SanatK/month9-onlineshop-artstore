package kg.attractor.artstore.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "reviews")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column
    private Integer artworkId;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String reviewBody;

    @NotNull
    @Column
    private Integer userId;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String userLogin;
}
