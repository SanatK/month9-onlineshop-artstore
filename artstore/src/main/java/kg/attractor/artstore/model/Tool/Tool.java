package kg.attractor.artstore.model.Tool;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "tools")
public class Tool {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String name;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String description;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private static final String CATEGORY = "tool";

    @NotNull
    @Column
    private int quantity;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String image;

    @NotNull
    @Column
    private float price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tool_type_id")
    private ToolType toolType;
}
