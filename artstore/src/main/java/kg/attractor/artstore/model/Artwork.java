package kg.attractor.artstore.model;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data @Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name = "artworks")
public class Artwork {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String name;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String image;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private final String CATEGORY = "artwork";

    @ManyToOne
    @JoinColumn(name = "artist_id")
    private Artist artist;

    @NotNull
    @Column
    private float price;

    @NotNull
    @Column
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;
}
