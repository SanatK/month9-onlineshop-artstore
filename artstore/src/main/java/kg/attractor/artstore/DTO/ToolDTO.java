package kg.attractor.artstore.DTO;
import kg.attractor.artstore.model.Tool.Tool;
import kg.attractor.artstore.model.Tool.ToolType;
import lombok.*;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ToolDTO {
    private Integer id;
    private String name;
    private String description;
    private int quantity;
    private String image;
    private float price;
    private ToolTypeDTO toolType;

    static ToolDTO from(Tool tool){
        return builder()
                .id(tool.getId())
                .name(tool.getName())
                .description(tool.getDescription())
                .quantity(tool.getQuantity())
                .image(tool.getImage())
                .price(tool.getPrice())
                .toolType(ToolTypeDTO.from(tool.getToolType()))
                .build();

    }


    @Data
    @Builder(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class ToolTypeDTO {
        private int id;
        private String typeOfTool;
        private List<Tool> tools;

        public static ToolTypeDTO from(ToolType toolType) {
            return builder()
                    .id(toolType.getId())
                    .typeOfTool(toolType.getTypeOfTool())
                    .tools(toolType.getTools())
                    .build();
        }
    }
}
