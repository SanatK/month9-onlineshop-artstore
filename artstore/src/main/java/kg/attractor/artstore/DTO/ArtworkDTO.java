package kg.attractor.artstore.DTO;
import kg.attractor.artstore.model.Artist;
import kg.attractor.artstore.model.Artwork;
import kg.attractor.artstore.model.Genre;
import lombok.*;
import java.util.List;


@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ArtworkDTO {
    private int id;
    private String name;
    private String image;
    private Artist artist;
    private float price;
    private int quantity;
    private GenreDTO genre;
    private String category;

    public static ArtworkDTO from(Artwork artwork){
        return builder()
                .id(artwork.getId())
                .name(artwork.getName())
                .image(createImagePath(artwork.getImage()))
                .artist(artwork.getArtist())
                .price(artwork.getPrice())
                .quantity(artwork.getQuantity())
                .genre(GenreDTO.from(artwork.getGenre()))
                .category(artwork.getCATEGORY())
                .build();
    }

    private static String createImagePath(String imageName){
        return String.format("/images/artworks/%s.jpg", imageName);
    }

    @Data
    @Builder(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class GenreDTO {
        private int id;
        private String name;
        private String description;

        public static GenreDTO from(Genre genre) {
            return builder()
                    .id(genre.getId())
                    .name(genre.getName())
                    .description(genre.getDescription())
                    .build();
        }
    }
}

