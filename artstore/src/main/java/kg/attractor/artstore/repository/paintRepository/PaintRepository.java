package kg.attractor.artstore.repository.paintRepository;

import kg.attractor.artstore.model.Paint.Paint;
import org.springframework.data.jpa.repository.JpaRepository;



public interface PaintRepository extends JpaRepository<Paint, Integer> {
}
