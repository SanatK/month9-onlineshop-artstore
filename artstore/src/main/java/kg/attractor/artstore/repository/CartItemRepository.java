package kg.attractor.artstore.repository;

import kg.attractor.artstore.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {
    List<CartItem> findAllByCart_Id(Integer cartId);

}
