package kg.attractor.artstore.repository.invoiceRepository;

import kg.attractor.artstore.model.Invoice.InvoiceItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceItemRepository extends JpaRepository<InvoiceItem, Integer> {
    List<InvoiceItem> findAllByInvoice_Id(Integer invoiceId);

}
