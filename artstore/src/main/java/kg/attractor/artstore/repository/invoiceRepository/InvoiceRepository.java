package kg.attractor.artstore.repository.invoiceRepository;

import kg.attractor.artstore.model.Invoice.Invoice;
import kg.attractor.artstore.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {
    List<Invoice> findAllByUser(User user);
}
