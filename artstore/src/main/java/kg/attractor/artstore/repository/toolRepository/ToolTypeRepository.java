package kg.attractor.artstore.repository.toolRepository;

import kg.attractor.artstore.model.Paint.Paint;
import kg.attractor.artstore.model.Tool.Tool;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ToolTypeRepository extends JpaRepository<Tool, Integer> {
}
