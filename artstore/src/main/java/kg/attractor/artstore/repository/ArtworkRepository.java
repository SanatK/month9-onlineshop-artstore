package kg.attractor.artstore.repository;

import kg.attractor.artstore.model.Artwork;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtworkRepository extends JpaRepository<Artwork, Integer> {
    Iterable<Artwork>findAllByName(String name);
    Artwork findAllById(Integer id);
}
