package kg.attractor.artstore.repository;

import kg.attractor.artstore.model.Artist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistRepository extends JpaRepository<Artist, Integer> {
}
