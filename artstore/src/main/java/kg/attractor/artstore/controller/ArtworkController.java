package kg.attractor.artstore.controller;

import kg.attractor.artstore.DTO.ArtworkDTO;
import kg.attractor.artstore.model.Artwork;
import kg.attractor.artstore.service.ArtworkService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class ArtworkController {

    private ArtworkService artworkService;

    public ArtworkController(ArtworkService artworkService) {
        this.artworkService = artworkService;
    }

    @GetMapping("/findArtworks/{text-to-search}")
    @ResponseBody
    public List<ArtworkDTO> findArtworks(@PathVariable("text-to-search") String textToSearch){
        return artworkService.findArtworks(textToSearch);
    }
}
