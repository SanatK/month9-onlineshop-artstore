package kg.attractor.artstore.controller;
import kg.attractor.artstore.DTO.UserDTO;
import kg.attractor.artstore.configuration.PasswordResetToken;
import kg.attractor.artstore.model.User;
import kg.attractor.artstore.repository.ResetRepository;
import kg.attractor.artstore.repository.UserRepository;
import kg.attractor.artstore.service.UserService;
import lombok.AllArgsConstructor;
import org.dom4j.rule.Mode;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.UUID;

@Controller
@RequestMapping
@AllArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserRepository userRepository;
    private final ResetRepository resetRepository;


    @GetMapping("/profile")
    public String userProfile(Model model, Principal principal){
        var user = userService.getByEmail(principal.getName());
        model.addAttribute("dto", user);
        return "profile";
    }

    @GetMapping("/register")
    public String getRegisterPage(Model model){
        return "register";
    }

    @PostMapping("/register")
    public String registerPage(@Valid UserDTO userDTO,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", userDTO);

        if(validationResult.hasFieldErrors()){
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/register";
        }
        userService.register(userDTO);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/recovery-password")
    public String recoveryPasswordPage(Model model){
        return "recovery";
    }

    @PostMapping("/recovery-password")
    public String confirmEmail(@RequestParam("email") String email,
                                           RedirectAttributes attributes) {

        if (!userRepository.existsByEmail(email)) {
            attributes.addFlashAttribute("errorText", "Entered email does not exist!");
            return "redirect:/";
        }

        PasswordResetToken pToken = PasswordResetToken.builder()
                .user(userRepository.findByEmail(email).get())
                .token(UUID.randomUUID().toString())
                .build();

        resetRepository.deleteAll();
        resetRepository.save(pToken);
        attributes.addAttribute("email", email);
        return "redirect:/reset-password";
    }

    @GetMapping("/reset-password")
    public String pageResetPassword(Model model) {
        PasswordResetToken token = resetRepository.findAll().get(0);
        String tokenS = token.getToken();
        model.addAttribute("token", tokenS);
        return "reset-password-page";
    }

    @PostMapping("/reset-password")
    public String submitResetPasswordPage(@RequestParam("token") String token,
                                          @RequestParam("newPassword") String newPassword,
                                          RedirectAttributes attributes) {

        if (!resetRepository.existsByToken(token)) {
            attributes.addFlashAttribute("errorText", "Incorrect email was entered");
            return "redirect:/reset-password";
        }

        PasswordResetToken pToken = resetRepository.findByToken(token).get();
        User user = userRepository.findById(pToken.getUser().getId()).get();
        user.setPassword(new BCryptPasswordEncoder().encode(newPassword));

        userRepository.save(user);

        return "redirect:/login";
    }

}
