package kg.attractor.artstore.controller;
import kg.attractor.artstore.model.Artwork;
import kg.attractor.artstore.model.Cart;
import kg.attractor.artstore.model.CartItem;
import kg.attractor.artstore.model.Invoice.Invoice;
import kg.attractor.artstore.repository.ArtworkRepository;
import kg.attractor.artstore.repository.CartRepository;
import kg.attractor.artstore.service.CartService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;


@Controller
@AllArgsConstructor
public class CartController {
    private final CartService cartService;
    private final ArtworkRepository artworkRepository;

    @GetMapping("/cart")
    public String getCurrentUserId(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        if(!userEmail.equals("anonymousUser")){
            List<CartItem> cartItemList = cartService.getThisUserCart(userEmail);
            if (cartItemList != null) {
                model.addAttribute("cartItems", cartItemList);
            }
        }
        return "cart";
    }

    @PostMapping("/cart/add")
    public String addToCart(@RequestParam Integer id, String category, int quantity, HttpSession session){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        cartService.addNewItem(id, category, quantity, userEmail);

        //хранение в сессии
        Artwork artwork = artworkRepository.findAllById(id);
        if (session != null) {
            var attr = session.getAttribute(Constants.CART_ID);
            if (attr == null) {
                session.setAttribute(Constants.CART_ID, new ArrayList<Artwork>());
            }
            try {
                var list = (List<Artwork>) session.getAttribute(Constants.CART_ID);
                list.add(artwork);
            } catch (ClassCastException ignored) {

            }
        }


        return "redirect:/cart";
    }

    @PostMapping("/cart/remove")
    public String removeItem(@RequestParam Integer itemId, HttpSession session){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        cartService.removeThisItem(itemId, userEmail);

        //удаление отдельных элементов из корзины в сессии
        if (session != null) {
            var attr = session.getAttribute(Constants.CART_ID);
            if (attr == null) {
                return "redirect:/cart";
            }
            try {
                var list = (List<Artwork>) session.getAttribute(Constants.CART_ID);
                for(int i = 0; i<list.size(); i++){
                    if(list.get(i).getId()==itemId){
                        list.remove(i);
                    }
                }
            } catch (ClassCastException ignored) {

            }
        }
        return "redirect:/cart";
    }

    @GetMapping("/order")
    public String getOrderPage(Model model){
        return "order";
    }


    @PostMapping("/order")
    public String order(@RequestParam String customerPhoneNumber, String customerAddress, Model model, HttpSession session){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        Invoice invoice = cartService.createNewOrder(userEmail,customerPhoneNumber, customerAddress);
        model.addAttribute("invoice", invoice);

        //очищение корзины в сессии
        session.removeAttribute(Constants.CART_ID);

        return "success";
   }

    @GetMapping("/success")
    public String getSuccessfulOrderPage(Model model){
        return "success";
    }
}


