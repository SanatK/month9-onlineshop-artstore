package kg.attractor.artstore.controller;

import kg.attractor.artstore.repository.ArtworkRepository;
import kg.attractor.artstore.service.ArtistService;
import kg.attractor.artstore.service.ArtworkService;
import kg.attractor.artstore.service.PropertiesService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping
@AllArgsConstructor
public class FrontendController {
    private final ArtworkRepository artworkRepository;
    private final ArtworkService artworkService;
    private final PropertiesService propertiesService;
    private final ArtistService artistService;

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri){
        if(list.hasNext()){
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if(list.hasPrevious()){
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }
        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size){
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }

    @GetMapping
    public String index(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        var artworks = artworkService.getArtworks(pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(artworks, propertiesService.getDefaultPageSize(), model, uri);
        return "index";
    }

    @GetMapping("/artists")
    public String artistsPage(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        var artists = artistService.getArtists(pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(artists, propertiesService.getDefaultPageSize(), model, uri);
        return "artists";
    }

    @GetMapping("/artworks/{id:\\d+?}")
    public String artworkPage(@PathVariable int id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        if(!userEmail.equals("anonymousUser")){
            model.addAttribute("Authorized", "true");
        }
        model.addAttribute("artwork", artworkService.getArtwork(id));
        return "artwork";
    }

}
