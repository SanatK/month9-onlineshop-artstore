package kg.attractor.artstore.controller;

import kg.attractor.artstore.model.Artwork;
import kg.attractor.artstore.model.Cart;
import kg.attractor.artstore.model.CartItem;
import kg.attractor.artstore.model.Invoice.Invoice;
import kg.attractor.artstore.model.Invoice.InvoiceItem;
import kg.attractor.artstore.model.Review;
import kg.attractor.artstore.repository.*;
import kg.attractor.artstore.repository.invoiceRepository.InvoiceItemRepository;
import kg.attractor.artstore.repository.invoiceRepository.InvoiceRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping
@AllArgsConstructor
public class ReviewController {
    InvoiceItemRepository invoiceItemRepository;
    InvoiceRepository invoiceRepository;
    UserRepository userRepository;
    ArtworkRepository artworkRepository;
    ReviewRepository reviewRepository;

    @PostMapping("/add-review")
    public String showAddReviewPage(@RequestParam Integer id, String category, Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        List<Invoice> invoice = invoiceRepository.findAllByUser(userRepository.findUserByEmail(userEmail));
        if(invoice!=null){
            for(int i = 0; i<invoice.size(); i++){
                List<InvoiceItem> a = invoiceItemRepository.findAllByInvoice_Id(invoice.get(i).getId());
                if(a!=null){
                    for(int z =0; z<a.size(); z++){
                        if(a.get(z).getItemCategory().equals(category)&&a.get(z).getItemName().equals(artworkRepository.findAllById(id).getName())){
                            model.addAttribute("AddReview", "true");
                            model.addAttribute("category", category);
                            model.addAttribute("id", id);
                        }
                    }
                }
            }
        }

        return "add-review";
    }

    @PostMapping("/new-review")
    public String addNewReview(@RequestParam Integer id, String category, String review){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        Integer thisUserId = userRepository.findByEmail(userEmail).get().getId();
        String userLogin = userRepository.findUserByEmail(userEmail).getLogin();
        Review review1 = Review.builder()
                            .reviewBody(review)
                            .userId(thisUserId)
                            .artworkId(id)
                            .userLogin(userLogin)
                            .build();
        reviewRepository.save(review1);
        return "redirect:/reviews/"+id;
    }

    @GetMapping("/reviews/{id:\\d+?}")
    public String artworkPage(@PathVariable int id, Model model) {
        List<Review> reviews = reviewRepository.findAllByArtworkId(id);
        if(reviews!=null){
            model.addAttribute("reviews", reviews);
        }
        return "reviews";
    }
}
