package kg.attractor.artstore.service;

import kg.attractor.artstore.DTO.ArtistDTO;
import kg.attractor.artstore.repository.ArtistRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ArtistService {
    private final ArtistRepository artistRepository;

    public Page<ArtistDTO> getArtists(Pageable pageable){
        return artistRepository.findAll(pageable)
                .map(ArtistDTO::from);
    }
}
