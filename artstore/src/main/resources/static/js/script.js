'use strict';

async function findArtworks(form) {
    let data = new FormData(form);
    let textToSearch = data.get("text-to-search").toString();
    let response = await fetch("http://localhost:8080/findArtworks/"+textToSearch);
    return await response.json();
}

function createImageDiv(image){
    let div_content = `<a href="/artworks/${image.id}">
                            <div class="flex-column box-400">
                                <img class="artwork-image" src="${image.image}" alt="${image.name}">
                                <p>${image.name}</p>
                            </div>
                        </a>`;
    let element = document.createElement(`div`);
    element.innerHTML += div_content;
    element.classList.add("flex", "flex-column", "box", "flex-v-center");
    return element;
}

class Image{
    constructor(id, image, name) {
        this.id = id;
        this.image = image;
        this.name = name;
    }
}

async function showImages(form) {
    let images = await findArtworks(form);
    document.getElementById("image-container").innerHTML = "";
    for(let i = 0; i<images.length; i++){
        let image = new Image(images[i].id, images[i].image, images[i].name);
        let elem = createImageDiv(image);
        document.getElementById("image-container").appendChild(elem);
    }
}