use `artstore`;
CREATE TABLE `artists` (
 `id` INT auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 `surname` varchar(128) NOT NULL,
 `age` int not null,
 `description` varchar(255) NOT NULL,
 `photo` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
);
create table `artworks` (
 `id` INT auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 `image` varchar(128) NOT NULL,
 `price` float not null,
 `quantity` int not null,
 `artist_id` int not null,
 PRIMARY KEY (`id`),
 CONSTRAINT `fk_art_artist`
 FOREIGN KEY (`artist_id`)
 REFERENCES `artists` (`id`)
);
