use `artstore`;

 CREATE TABLE `carts` (
 `id` INT auto_increment NOT NULL,
 `user_id` INT NOT NULL,
 PRIMARY KEY (`id`)
 );

 CREATE TABLE `cart_items`(
 `id` INT auto_increment NOT NULL,
 `item_name` varchar(128) NOT NULL,
 `quantity` INT NOT NULL,
 `price_per_one` float not null,
 `price` float not null,
 `item_category` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
 );

alter table `cart_items`
 add column `cart_id` INT NOT NULL after `quantity`,
 add CONSTRAINT `fk_cart_item__cart`
 FOREIGN KEY (`cart_id`)
 REFERENCES `carts` (`id`);