use `artstore`;

insert into `artists` (`name`, `surname`, `age`, `description`, `photo`) values
('Banksy', 'B', 33, 'The British artist, political activist',
  'Banksy'),
('Darryl "CornBread"', 'McCray', 54, 'Born Darryl McCray, Cornbread is generally acknowledged to be the first modern graffiti artist',
  'Darryl-CornBread'),
('Jean-Michel', 'Basquiat', 41, 'Among the most famous contemporary artists of all time',
  'Basquiat-Jean-Michel'),
('Shepard', 'Fairey', 48, 'In 1989, a skateboarding enthusiast and Rhode Island School of Design student named Shepard Fairey started to post stickers featuring the face of the famed professional wrestler.',
  'Shepard-Fairey');

insert into `artworks` (`name`, `image`, `price`, `quantity`, `artist_id`, `genre_id`) values
('All eyes on you', 'all-eyes-on-you', 1200, 5, 1, 1),
('Bomb Head', 'bomb-head', 850, 5, 1, 2),
('Child Soldier', 'child-soldier', 900, 10, 2, 3),
('Clacton on Sea', 'clacton-on-sea', 1200, 5, 1, 1),
('Colourful women', 'colourful-woman', 1000, 2, 3, 5),
('Dismaland', 'dismaland', 600, 12, 4, 4),
('Girl with balloon', 'girl-with-balloon', 1500, 1, 2, 1),
('Girl with Rose', 'girl-with-rose', 860, 6, 3, 4),
('Golden Future', 'golden-future', 930, 9, 2, 2),
('HOPE', 'hope-obama', 400, 20, 4, 3),
('Hung Lover', 'hung-lover', 300, 15, 3, 5),
('Olympic', 'olympic-rings', 650, 5, 1, 1),
('Cat', 'pink-cat', 210, 20, 4, 2),
('Queen', 'posters-queen', 1000, 5, 4, 2),
('Power', 'power-equality', 600, 12, 3, 1),
('Pulp Fiction', 'pulp-fiction-banana', 800, 6, 4, 4),
('Remove', 'removal-art-prints', 1350, 2, 3, 3),
('City', 'urban-city', 250, 12, 3, 1);
