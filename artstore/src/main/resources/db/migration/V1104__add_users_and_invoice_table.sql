use `artstore`;

CREATE TABLE `users` (
 `id` INT auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 `surname` varchar(128) NOT NULL,
 `email` varchar(128) NOT NULL,
 `login` varchar(128) NOT NULL,
 `password` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
);

CREATE TABLE `invoices` (
 `id` INT auto_increment NOT NULL,
 `phone_number` varchar(128) NOT NULL,
 `address` varchar(128) NOT NULL,
 `sum_price` float NOT NULL,
 `invoice_date` date NOT NULL,
 `invoice_time` time NOT NULL,
  PRIMARY KEY (`id`)
);

alter table `invoices`
 add column `user_id` INT NOT NULL after `id`,
 add CONSTRAINT `fk_invoice__user`
 FOREIGN KEY (`user_id`)
 REFERENCES `users` (`id`);