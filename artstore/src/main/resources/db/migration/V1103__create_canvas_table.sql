use `artstore`;

CREATE TABLE `canvases` (
 `id` INT   auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 `image` varchar(128) NOT NULL,
 `length` int NOT NULL,
 `width` int NOT NULL,
 `price` float NOT NULL,
 `quantity` int NOT NULL,
 PRIMARY KEY (`id`)
);

