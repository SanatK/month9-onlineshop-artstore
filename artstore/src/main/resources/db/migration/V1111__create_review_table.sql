use `artstore`;


CREATE TABLE `reviews` (
`id` INT auto_increment NOT NULL,
`review_body` varchar(128) NOT NULL,
`artwork_id` INT NOT NULL,
`user_id` INT NOT NULL,
`user_login` varchar(128) NOT NULL,
PRIMARY KEY(`id`)
);